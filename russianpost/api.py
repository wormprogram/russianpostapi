# -*- coding: utf-8 -*-

from suds.client import Client
from russianpost.exception import ApiException


class RussianPostApi():
    def __init__(self):
        self.wsdl_uri = 'http://voh.russianpost.ru:8080/niips-operationhistory-web/OperationHistory?wsdl'
        self.client_api = None

    def track(self, tracking):
        try:
            client_api = self.get_client_api()
            response = client_api.service.GetOperationHistory(tracking, '0')
        except Exception:
            raise ApiException('Error API.')

        return response

    def get_client_api(self):
        if self.client_api is None:
            self.client_api = Client(self.wsdl_uri)
        return self.client_api